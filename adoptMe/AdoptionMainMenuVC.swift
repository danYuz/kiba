 //
//  AdoptionMainMenuVC.swift
//  adoptMe
//
//  Created by Daniel Ramirez on 1/26/17.
//  Copyright © 2017 simpleCoding. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire
import AlamofireImage
import CoreLocation
import MapKit
import FirebaseMessaging
import FirebaseInstanceID

class AdoptionMainMenuVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate{
    
    @IBOutlet weak var searchmenu: UIBarButtonItem!
    @IBOutlet weak var showmenu: UIBarButtonItem!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //slide top
    private let refreshControl = UIRefreshControl()
    
    let manager = CLLocationManager()
    
    var arrJSON:[String] = []
    var imgPath:String = ""
    var arrayDogges : [Dogges] = []
    var arraySponsores : [ModelFirstSpon] = []
    var arrayOrg: [ModeloOrganizacion] = []
    var arrayRaza: [ModeloRaza] = []
    var index = IndexPath()
    var arrayFavorites = [Int]()
    var nestedArray: [AnyObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //prueba 2 anuncio
        
        
        //refresh data slide top
        refreshControl.tintColor = UIColor.darkGray
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(AdoptionMainMenuVC.populate), for: UIControlEvents.valueChanged)
        
        
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }
        
        //side  menu
        searchmenu.target = self.revealViewController()
        searchmenu.action = #selector(SWRevealViewController.rightRevealToggle(_:))
        
        //menubt action
        showmenu.target = self.revealViewController()
        showmenu.action = #selector(SWRevealViewController.revealToggle(_:))
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        //initializers
        collectionView.delegate = self
        collectionView.dataSource = self
        
        FIRMessaging.messaging().subscribe(toTopic: "/topic/news")
        collectionView.reloadData()
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        collectionView.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        //call populate func to download the information
        populate()

    }
    
    //logout button , erase all the credentials information
    @IBAction func logOutButtonPressed(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: UID_KEY)
        dismiss(animated: true, completion: nil)
    }
    
    //func called to download AllPets information
    func downloadDogDetails(completed: @escaping DownloadCompleted){
        
        let currentUrl = mainViewDogsUrl
        //we empty the arrays so we can re-used them without override
        self.arrayDogges = []
        self.arrayOrg = []
        self.arrayRaza = []
        
        //called to download AllPets information
        Alamofire.request(currentUrl).responseJSON { response in
            //result.value contains the information  and we parse it to an Array[Dictionary<String,AnyObject>]
            let result = response.result
            
            //message alert to user, internet problems
            if response.value == nil{
                self.createAlert(title: "ups", message: "we detected some trouble with your connection!.")
            }
            //dictionary if result.value existe
            if let dict = result.value as? [Dictionary<String,AnyObject>]{
                
                //obj in dict
                for obj in dict{
                    let objDog = Dogges(objDog: obj)
                    let objOrg = ModeloOrganizacion(orgObj: obj)
                    let objRaza = ModeloRaza(razaObj: obj)
                    
                    
                    
                    self.arrayOrg.append(objOrg)
                    self.arrayRaza.append(objRaza)
                    self.arrayDogges.append(objDog)
                    //nested array
                    self.nestedArray.append(objDog)
                    
                    //reload the data
                    self.collectionView.reloadData()
                    
                }
                
            }
        }
        completed()
    
    }

    
    //func called to download AllAds information
    func downloadSponsores(completed: @escaping DownloadCompleted){
        
        
        let currentUrl = mainSponsoresUrl
        //we empty the arrays so we can re-used them without override
        self.arraySponsores = []
        
        //called to download AllPets information
        Alamofire.request(currentUrl).responseJSON { response in
            //result.value contains the information  and we parse it to an Array[Dictionary<String,AnyObject>]
            let result = response.result
            
            if response.value == nil{
                self.createAlert(title: "ups", message: "we detected some trouble with your connection!.")
            }
            
            if let dict = result.value as? [Dictionary<String,AnyObject>]{
                
//                self.saveDogDictLocal(dict: dict)
                //object Dooges created
                for obj in dict{
                    let objSpon = ModelFirstSpon(sponsore: obj)
    
                    self.arraySponsores.append(objSpon)
                    self.nestedArray.append(objSpon)
                    
                    //reload the data
                    self.collectionView.reloadData()
                }
            }
            
        }
        completed()
    }
    
    //func populate the Arraysfor the View.
    func populate(){
        nestedArray = []
        downloadDogDetails {   
            self.collectionView.reloadData()
            self.refreshControl.endRefreshing()
        }
        downloadSponsores {
            self.collectionView.reloadData()
            self.refreshControl.endRefreshing()
        }
        
    }
    
    //didselected
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //did selected code here
        
        performSegue(withIdentifier: TO_DETAIL_SEGUE, sender: nestedArray[indexPath.row])
        
    }
    //numb of rows in section
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return nestedArray.count
    }
    
    //cellForIndex
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let obj = nestedArray[indexPath.row]
        
        switch obj {
        case is Dogges:
            var arrayImgPath = ""
            
            let objPlaceHolder = nestedArray[indexPath.row] as! Dogges
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"mainMenuCell", for: indexPath) as! AdoptionMainCell
            
            //get image and show it
            if objPlaceHolder.imgPath.isEmpty{
                arrayImgPath = retUrl(tipo: objPlaceHolder.tipo)
            }else{
                arrayImgPath = BASE + objPlaceHolder.imgPath[0]
            }
            print("\(objPlaceHolder.imgPath[0])")
            cell.confCell(dogges: obj as! Dogges)
            cell.imagenView.af_setImage(withURL: URL(string: arrayImgPath)!, placeholderImage: retImg(name: objPlaceHolder.tipo))
            
            return cell
           
        case is ModelFirstSpon:
            var arrayImgPath = ""
            let objPlaceHolder = nestedArray[indexPath.row] as! ModelFirstSpon
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SponsoredMainMenuCell", for: indexPath) as! SponsoresCell
            
            if objPlaceHolder.imgPath == ""{
                arrayImgPath = "catDefault.jpg"
            }else{
                arrayImgPath = BASE + objPlaceHolder.imgPath
            }
            print("\(BASE + objPlaceHolder.imgPath)")
            
            cell.confCell(spon: obj as! ModelFirstSpon)
            cell.sponsImagenView.af_setImage(withURL: URL(string:arrayImgPath)!, placeholderImage: retImg(name: objPlaceHolder.tipo))
            
            return cell
            
        case is ModelSecondSpon:
            var arrayImgPath = ""
            let objPlaceHolder = nestedArray[indexPath.row] as! ModelSecondSpon
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SecondSponsoredMainMenuCell", for: indexPath) as! SponsoresCell
            
            if objPlaceHolder.imgPath == ""{
                arrayImgPath = "catDefault.jpg"
            }else{
                arrayImgPath = objPlaceHolder.imgPath
            }
            cell.sponsImagenView.af_setImage(withURL: URL(string:arrayImgPath)!, placeholderImage: retImg(name: objPlaceHolder.tipo))
            
            return cell
        default:
            
            return UICollectionViewCell()
     
        }
       
    }
    
    //return arrayUrl img paths
    func returnArrayUrlImg(array:[Dogges]) -> [URL]{
        
        var arrayCreated:[URL] = []
        
        for dog in array{
            var imgPath = dog.imgPath
            
            if imgPath.isEmpty{
                if dog.tipo == "Perro"{
                    imgPath.append("dogDefault.jpg")
                }else{
                    //los anuncion se les mostrara este default, terminar programacion
                    imgPath.append("catDefault.jpg")
                }
            }
            let imgUrl = URL(string: imgPath[0])!
            arrayCreated.append(imgUrl)
        }
        return arrayCreated
    }
    
    
    //func return imgUrlSponsores
    func returnArraySponsUrlImg(array:[ModelFirstSpon]) -> [URL]{
        //array to return
        var arrayCreated:[URL] = []
        
        //spon in array
        for spon in array{
            //variable to store o assign the value of the onj imgPath
            var imgPathSpon = spon.imgPath
            
            //if is null
            if imgPathSpon == ""{
                //we assign the value Default
                imgPathSpon = "dogDefault.jpg"
            }
            
            //constant to store imgPath : URL
            let imgUrl = URL(string: imgPathSpon)!
            
            //append to arrayCreated Urls
            arrayCreated.append(imgUrl)
        }
        return arrayCreated
    }
    func retUrl(tipo: String) -> String{
        
        if tipo == "Perro"{
            return "dogDefault.jpg"
        }else{
            return "catDefault.jpg"
        }
        
    }
    //return image holder
    func retImg(name:String) -> UIImage{
        
        if name == "Perro"{
            return #imageLiteral(resourceName: "dogDefault")
        }else{
            return #imageLiteral(resourceName: "catDefault")
        }
        
        
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func saveDogDictLocal(dict: [Dictionary<String,AnyObject>]){
        
        UserDefaults.standard.set(dict, forKey: ID_ARRAYDOGS)
        print("INFO: Array information stored locally succeed \(dict)")
    }
    
    //func prepare segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //destination send to detailDog information
        if let destination = segue.destination as? DetailDogInfoVC{
            if let dog = sender as? Dogges{
                destination.dog = dog
            }
            
        }
    }
    //create new alert function
    func createAlert(title:String, message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func tapAnimation(){
        
    }
}
