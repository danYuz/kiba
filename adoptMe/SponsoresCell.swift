//
//  SponsoresCell.swift
//  adoptMe
//
//  Created by Daniel Ramirez on 3/28/17.
//  Copyright © 2017 techdevius. All rights reserved.
//

import UIKit

class SponsoresCell: UICollectionViewCell {
    
    
    @IBOutlet weak var website: StyleLabel!
    @IBOutlet weak var sponName: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var cellOrPrice: UILabel!
    @IBOutlet weak var sponsImagenView: UIImageView!
    
    func confCell(spon: ModelFirstSpon){
       
        sponName.text = spon.r1
        message.text = spon.r2
        cellOrPrice.text = spon.r3
        website.text = spon.r4
        
        
    
    }
    
}
