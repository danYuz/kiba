//
//  ModelMascotasAvistadas.swift
//  adoptMe
//
//  Created by Daniel Ramirez on 4/17/17.
//  Copyright © 2017 techdevius. All rights reserved.
//

import Foundation

class ModelMascotasAvistadas {
    private var _nombre: String!
    private var _tipos: String!
    private var _sexo: String!
    private var _edad:String!
    private var _datos: String!
    private var _correo: String!
    
    
    var nombre:String{
        return _nombre
    }
    var tipos:String{
        return _tipos
    }
    var sexo:String{
        return _sexo
    }
    var edad:String{
        return _edad
    }
    var datos:String{
        return _datos!
        
    }
    var correo:String{
        return _correo
    }
    
    init(nombre:String,tipos:String,sexo:String,edad:String,datos:String,correo:String) {
        
        self._nombre = nombre
        self._edad = edad
        self._correo = correo
        self._sexo = sexo
        self._tipos = tipos
        self._datos = datos
        
    }
}
