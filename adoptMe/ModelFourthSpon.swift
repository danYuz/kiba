//
//  ModelFourthSpon.swift
//  adoptMe
//
//  Created by Daniel Ramirez on 4/17/17.
//  Copyright © 2017 techdevius. All rights reserved.
//

import Foundation
class ModelFourthSpon {
    private var _imagenUrlPrincipal:String!
    private var _imagenUrlEncabezados:String!
    private var _encabezado:String!
    private var _subtitulo:String!
    private var _telefono:String!
    private var _ubicaion:String!
    private var _parrafo:String!
    private var _paginaWeb:String!
    private var _pieDePagUrl:String!
    
    var imagenUrlPrincipal:String{
        return _imagenUrlPrincipal
    }
    var imagenUrlEncabezado:String{
        return _imagenUrlEncabezados
    }
    var encabezado:String{
        return _encabezado
    }
    var subtitulo:String{
        return _subtitulo
    }
    var telefono:String{
        return _telefono
    }
    var ubicacion:String{
        return _ubicaion
    }
    var parrafo:String{
        return _parrafo
    }
    var pieDePagUrl:String{
        return _pieDePagUrl
    }
    init(imagenPrincipal:String, imagenEncabezado:String,encabezado:String,subtitulo:String,telefono:String,ubicacion:String,parrafo:String, paginaWeb:String,pieDePag:String) {
        
        self._imagenUrlPrincipal = imagenPrincipal
        self._imagenUrlEncabezados = imagenEncabezado
        self._encabezado = encabezado
        self._subtitulo = subtitulo
        self._telefono = telefono
        self._ubicaion = ubicacion
        self._parrafo = parrafo
        self._pieDePagUrl = pieDePag
        
        
    }
}
