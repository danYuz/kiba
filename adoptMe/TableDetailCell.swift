//
//  TableDetail.swift
//  adoptMe
//
//  Created by Daniel Ramirez on 3/8/17.
//  Copyright © 2017 techdevius. All rights reserved.
//

import UIKit

class TableDetailCell: UITableViewCell {

    @IBOutlet weak var key: UILabel!
    @IBOutlet weak var value: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    
    
    func confCell(keyName:String,valueName:String){
        
        key.text = keyName
        value.text = valueName
        
        
    }
    
    func syleCell(cell: UITableViewCell){
        
        cell.isUserInteractionEnabled = false
        cell.tintColor = UIColor.white
        
        //lables design
        key.textColor = UIColor.black
        value.textColor = UIColor(colorLiteralRed: 152.00/255.00, green: 68.00/255.00, blue: 252.00/255.00, alpha: 0.65)
        

    }
    
    

}
