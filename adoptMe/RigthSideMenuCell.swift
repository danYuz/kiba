//
//  RightSideMenuCell.swift
//  adoptMe
//
//  Created by Daniel Ramirez on 3/2/17.
//  Copyright © 2017 techdevius. All rights reserved.
//

import UIKit

class RightSideMenuCell: UITableViewCell {
    
    //right side
    @IBOutlet weak var imgRight: UIImageView!
    @IBOutlet weak var labelRight: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func conf(itemName: String,img:UIImage){
        self.labelRight.text = itemName
        self.imgRight.image = img
    }
}
