
//  Dogges.swift
//  adoptMe
//
//  Created by Daniel Ramirez on 1/31/17.
//  Copyright © 2017 simpleCoding. All rights reserved.
//


import UIKit
import Alamofire
import AlamofireImage

class Dogges{
   
    struct Keys {
        static let favArray = "favArray"
    }

    private var _id:Int!
    private var _nameDog:String!
    private var _description:String!
    private var _raza:Dictionary<String,AnyObject>!
    private var _edad:String!
    private var _org:Dictionary<String,AnyObject>!
    private var _sexo:String!
    private var _necesidades_especiales:String!
    private var _imgPaths:[String]!
    private var _tipo:String!
    private var _vacunado:Bool!
    private var _esterilizado:Bool!
    private var _favorite:Bool!
    
    var favorite:Bool{
        get{
           return _favorite
        }
        set{
            _favorite = newValue
        }
    }
    var vacunado:Bool{
        return _vacunado
    }
    
    var esterilizado:Bool{
        return _esterilizado
    }
    
    var id:Int{
        return _id
    }
    
    var tipo:String{
       if _tipo == ""{
            _tipo = "por asignar"
        }
        return _tipo
    }
    var imgPath:[String]{
        get {
           return _imgPaths
        }
    }
    
    var nameDog:String{
        if _nameDog == ""{
            _nameDog = "por asignar"
        }
        return _nameDog
    }
    
    var description:String{
        if _description == ""{
            _description = "por asignar"
        }
        return _description
    }
    
    var raza:Dictionary<String,AnyObject>{
        return _raza
    }
    
    
    var edad:String{
        if _edad == ""{
            _edad = "por asignar"
        }
        return _edad
    }
    
    var org:Dictionary<String,AnyObject>{
        return _org
    }
    
    
    var sexo:String{
        if _sexo == ""{
            _sexo = "por asignar"
        }
        return _sexo
    }
    
    var necesidades_especiales:String{
        if _necesidades_especiales == ""{
            _necesidades_especiales = "por asignar"
        }
        return _necesidades_especiales
    }
    
    required init?(coder aDecoder: NSCoder) {
        
    }
    
    func encode(with aCoder: NSCoder) {
        
    }
    
    init(objDog: Dictionary<String,AnyObject>){
        
       
        self._favorite = false
        
        if let name = objDog["nombre"] as? String{
            self._nameDog = name
            
        }
        if let edad = objDog["edad"] as? String{
            self._edad = edad
            
        }
        if let historia = objDog["historia"] as? String{
            self._description = historia
            
        }
        if let org = objDog["organizacion"] as? Dictionary<String,AnyObject>{
            self._org = org
            
        }
        if let sexo = objDog["sexo"] as? String{
            self._sexo = sexo
            
        }
        if let raza = objDog["raza"] as? Dictionary<String,AnyObject>{
            self._raza = raza
            
        }
        if let nece_espe = objDog["necesidades_especiales"] as? String{
            self._necesidades_especiales = nece_espe
            
        }
        if let imgPaths = objDog["imagenes_mascota"] as? [String]{
            self._imgPaths = imgPaths
            
        }
        if let tipo  = objDog["tipo"] as? String{
            self._tipo = tipo
            
        }
        if let id = objDog["id"] as? Int{
            self._id = id
            
        }
        if let esterilizadoData = objDog["esterilizado"] as? Bool{
            self._esterilizado = esterilizadoData
            
        }
        if let vacunadoData = objDog["vacunado"] as? Bool{
            self._vacunado = vacunadoData
            
        }
        
    }
    init() {
        
    }
}
