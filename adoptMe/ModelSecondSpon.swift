//
//  ModelSecondSponVC.swift
//  adoptMe
//
//  Created by Daniel Ramirez on 4/2/17.
//  Copyright © 2017 techdevius. All rights reserved.
//

import Foundation

class ModelSecondSpon{
    
    
    private var _tipo:String!
    private var _imgPaths:String!
    
    var tipo:String{
        return _tipo
    }
    var imgPath:String{
        return _imgPaths
    }
    init(SponsDic: Dictionary<String,AnyObject>) {
        self._tipo = "spons2"
        if let paths = SponsDic["anuncioImagenUrl"] as? String{
            self._imgPaths = paths
        }
    }
}
