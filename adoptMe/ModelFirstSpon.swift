
//  FirstSponVC.swift
//  adoptMe
//
//  Created by Daniel Ramirez on 3/31/17.
//  Copyright © 2017 techdevius. All rights reserved.
//

import Foundation

//
//  Raza.swift
//  adoptMe
//
//  Created by Daniel Ramirez on 2/8/17.
//  Copyright © 2017 simpleCoding. All rights reserved.

class ModelFirstSpon{
    
    private var _imgPath : String!
    private var _tipo : String!
    private var _id : Int!
    private var _r1 : String!
    private var _r2 : String!
    private var _r3 : String!
    private var _r4 : String!
    
    var imgPath:String{
        return _imgPath
    }
    var tipo:String{
        return _tipo
    }
    var id:Int{
        return _id
    }
    var r1:String{
        return _r1
    }
    var r2:String{
        return _r2
    }
    var r3:String{
        return _r3
    }
    var r4:String{
        return _r4
    }
    
    
    init() {
        
    }
    init(sponsore:Dictionary<String,AnyObject>){
        self._tipo = "spon1"
        
        if let imgPath = sponsore["anuncioImagenUrl"] as? String{
            self._imgPath = imgPath
            
        }
        if let id = sponsore["id"] as? Int{
            self._id = id
            
        }
        if let renglon1 = sponsore["anuncio1Renglon1"] as? String{
            self._r1 = renglon1
            
        }
        if let renglon2 = sponsore["anuncio1Renglon2"] as? String{
            self._r2 = renglon2
            
        }
        if let renglon3 = sponsore["anuncio1Renglon3"] as? String{
            self._r3 = renglon3
            
        }
        if let renglon4 = sponsore["anuncio1Renglon4"] as? String{
            self._r4 = renglon4
            
        }
    }
}
