//
//  AdoptionMainCell.swift
//  adoptMe
//
//  Created by Daniel Ramirez on 1/28/17.
//  Copyright © 2017 simpleCoding. All rights reserved.
//

import UIKit
import Foundation

class AdoptionMainCell: UICollectionViewCell {
    
    @IBOutlet weak var raza: UILabel!
    @IBOutlet weak var imgGender: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var orgName: UILabel!
    @IBOutlet weak var imagenView: UIImageView!
    
    func confCell(dogges: Dogges){
        let sexo = dogges.sexo
        name.text = "\(dogges.nameDog)"
        
        if let razaArray = dogges.raza as? Dictionary<String,AnyObject>{
            raza.text = razaArray["nombre_raza"] as? String
            
            if sexo == "Hembra"{
                imgGender.image = UIImage(named: "gender_female")
            }else{
                imgGender.image = UIImage(named: "gender_male")
            }
        }else{
            raza.text = "raza por asignar"
        }
        
        if let org = dogges.org as? Dictionary<String, AnyObject>{
            
            orgName.text = org["nombre"] as? String
            print("aqui aqui \(orgName.text)")
        }
        
        
    }
    
    
}
