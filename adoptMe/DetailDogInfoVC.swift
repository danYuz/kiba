//
//  DetailDogInfoVC.swift
//  adoptMe
//
//  Created by Daniel Ramirez on 3/1/17.
//  Copyright © 2017 techdevius. All rights reserved.
///Users/ElbuenYuz/Desktop/adoptMe/adoptMe/DetailDogInfoVC.swift

import UIKit

class DetailDogInfoVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource{
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    //objects from the dog detail label
    @IBOutlet weak var sexoDogLabel: UILabel!
    @IBOutlet weak var razaDogLabel: UILabel!
    @IBOutlet weak var nameDogLabel: UILabel!
    
    //objects from the 3 imageView
    @IBOutlet weak var vacunadoImgView: UIImageView!
    @IBOutlet weak var typeAgeImgView: UIImageView!
    @IBOutlet weak var esterilImgView: UIImageView!
    @IBOutlet weak var favorite: UIImageView!
    
    
    //objects from the information label
    @IBOutlet weak var historiasLabel: UILabel!
    @IBOutlet weak var historiasInfoTextView: UILabel!
    
    //object from organization label
    @IBOutlet weak var orgName: UILabel!
    @IBOutlet weak var orgEmail: UILabel!
    @IBOutlet weak var orgCell: UILabel!
    @IBOutlet weak var orgAddress: UILabel!
    
    var arrayDogDetailInformation:Dictionary<String,AnyObject> = [:]
    var dog = Dogges()
    var arrayImgUrl = [URL]()
    var razaObjeto = ModeloRaza()
    var arrayFavorites = [Int]()
    var org = ModeloOrganizacion()
    
    
    let tableArray = ["nombre_raza","temperamento","tamano","cuidados","id","key"]
    
    struct Keys {
        static var favs = "favs"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //load the information of the favs to the array
        if let array:[Int] =  UserDefaults.standard.array(forKey: Keys.favs) as? [Int]{
            arrayFavorites = array 
        }
        
        arrayImgUrl = []
        //poblate the information
        resultEstAgeShoot(dog: dog)
        org = ModeloOrganizacion(orgObj: dog.org)
        razaObjeto = ModeloRaza(razaObj: dog.raza)
        arrayDogDetailInformation = dog.raza
        
        //bannerInformation
        dogBannerInfo(nombre:dog.nameDog, raza:razaObjeto.nombre_raza, sexo: dog.sexo)
        orgBannerInfo(name: org.nombre)
        historiasInfoTextView.text = dog.description
       
        //imgUrlArray
        for img in dog.imgPath{
            let imgUrl = URL(string: img)!
            arrayImgUrl.append(imgUrl)
            
        }
        collectionView.delegate = self
        collectionView.dataSource = self
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        
    }
    //favorite action recognized
    @IBAction func favoriteTap(_ sender: UITapGestureRecognizer) {
        
        switch dog.favorite {
        case false:
            dog.favorite = true
            favorite.image = #imageLiteral(resourceName: "Kiba-Icon-Fav-2")
            addFavorite(dog: dog)
            saveFavsLocal(dict: arrayFavorites)
            
        default:
            dog.favorite = false
            favorite.image = #imageLiteral(resourceName: "Kiba-Icon-Fav-1")
            removeFavorite(dog: dog)
        }
        
       
        
        
    }
    
    func resultEstAgeShoot(dog:Dogges){
        print("dog edad type: \(dog.edad)")
        print("dog ester type: \(dog.esterilizado)")
        print("dog vac type: \(dog.vacunado)")
        
        switch dog.edad {
        case "Bebe":
            typeAgeImgView.image = #imageLiteral(resourceName: "Kiba-Icon-Age-1")
        case "Joven":
            typeAgeImgView.image = #imageLiteral(resourceName: "Kiba-Icon-Age-2")
        case "Adulto":
            typeAgeImgView.image = #imageLiteral(resourceName: "Kiba-Icon-Age-3")
        case "Anciano":
            typeAgeImgView.image = #imageLiteral(resourceName: "Kiba-Icon-Age-4")
        default:
            break
        }
        
        switch dog.vacunado {
        case true:
            vacunadoImgView.image = #imageLiteral(resourceName: "Kiba-Icon-Vacunado-2")
        default:
            vacunadoImgView.image = #imageLiteral(resourceName: "Kiba-Icon-Vacunado-1")
        }
        
        switch dog.esterilizado {
        case true:
            esterilImgView.image = #imageLiteral(resourceName: "Kiba-Icon-Esterilizado-2")
        default:
            esterilImgView.image = #imageLiteral(resourceName: "Kiba-Icon-Esterilizado-1")
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //did selected
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayImgUrl.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailDogCell", for: indexPath) as? DetailDogCell{
            var path = [String]()
            if arrayImgUrl == []{
                path.append("catDefault.jpg")
            }else{
                path = arrayImgUrl.map({"\(BASE)\($0)"})
                
            }
            cell.imgView.af_setImage(withURL: URL(string:path[0])!, placeholderImage:#imageLiteral(resourceName: "catDefault"))
            return cell
        }else{
            return UICollectionViewCell()
        }
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func dogBannerInfo(nombre: String, raza: String, sexo: String){
        //prueba
        nameDogLabel.text = nombre
        razaDogLabel.text = raza
        sexoDogLabel.text = sexo
    }
    //banner information
    func orgBannerInfo(name:String){
        orgName.text = name
        
    }
    //saveArrayIdFavs
    func saveFavsLocal(dict: [Int]){
        
        UserDefaults.standard.set(dict, forKey: Keys.favs)
        print("INFO: Array information stored locally succeed \(dict)")
    }
    //func prepare segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //destination send to detailDog information
        if let destination = segue.destination as? AdoptionMainMenuVC{
            if let idArray = sender as? [Int]{
                destination.arrayFavorites = idArray
            }
            
        }
    }
    //favorites func add element
    func addFavorite(dog: Dogges){
        if let index = arrayFavorites.index(where: { $0 == dog.id}){
            print("this animal is already a favorite")
        }else{
           arrayFavorites.append(dog.id)
        }
        
        print(dog.id)
        print("arrayFav true \(arrayFavorites)")
    }
    //favorites func remove element
    func removeFavorite(dog: Dogges){
        if let index = arrayFavorites.index(where: { $0 == dog.id}){
            arrayFavorites.remove(at: index)
        }
        print("arrayFav false \(arrayFavorites)")
    }
}

