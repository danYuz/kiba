//
//  ContactanosVC.swift
//  adoptMe
//
//  Created by Daniel Ramirez on 2/17/17.
//  Copyright © 2017 simpleCoding. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ContactVC: UIViewController,CLLocationManagerDelegate {
    //map
    @IBOutlet weak var map: MKMapView!
    let manager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
    // Do any additional setup after loading the view.
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()

    }
    
    @IBAction func doneBtnAcctionPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)   
    }
//    func goToView(name: String){
//        
//        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = mainStoryBoard.instantiateViewController(withIdentifier: name) as! AdoptionMainMenuVC
//        
//        self.navigationController?.pushViewController(vc, animated: true)
//    }
    //locationManager
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //we save the location of the array location
        let location = locations[0]
        //zoom
        let span:MKCoordinateSpan = MKCoordinateSpanMake(0.01, 0.01)
        //mylocation
        let myLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        //region
        let region:MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
        map.setRegion(region, animated: true)
        //show
        self.map.showsUserLocation = true
        
        print(location.altitude)
        print(location.speed)
    }
}
