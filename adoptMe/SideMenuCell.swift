//
//  SideMenuCell.swift
//  adoptMe
//
//  Created by Daniel Ramirez on 2/23/17.
//  Copyright © 2017 simpleCoding. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {

    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    func conf(itemName: String,img:UIImage ,cell: SideMenuCell){
        
            self.itemLabel.text = itemName
            self.imgView.image = img
        
        
    }
    
    func confTitle(itemName:String,Cell:SideMenuCell){
        
        
            self.itemLabel.text = itemName

    }
    
    func titleCellConf(cell: UITableViewCell){
        
    
        if cell.reuseIdentifier == "TitleCell"{
            
            cell.isUserInteractionEnabled = false
            cell.tintColor = UIColor.lightGray
            itemLabel.textColor = UIColor.black
        }else{
            cell.isUserInteractionEnabled = true
            cell.tintColor = UIColor.black
            cell.backgroundColor = UIColor.gray
            
            itemLabel.textColor = UIColor.white
        }
    
        
    }
}
