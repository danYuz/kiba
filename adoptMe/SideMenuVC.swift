//
//  SideMenuVC.swift
//  adoptMe
//
//  Created by Daniel Ramirez on 2/23/17.
//  Copyright © 2017 simpleCoding. All rights reserved.
//

import UIKit

class SideMenuVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var tableArray = [String]()
    var imgArray = [UIImage]()
    override func viewDidLoad() {
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableArray = ["Feedback","Configuracion","Donaciones","Logout","Mis Favoritos","Adopta","Historias"]
        imgArray = [#imageLiteral(resourceName: "Kiba-Icono-1"),#imageLiteral(resourceName: "Kiba-Icono-2"),#imageLiteral(resourceName: "Kiba-Icono-3"),#imageLiteral(resourceName: "Kiba-Icono-4"),#imageLiteral(resourceName: "Kiba-Icono-5"),#imageLiteral(resourceName: "Kiba-Icono-6"),#imageLiteral(resourceName: "Kiba-Icono-8")]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier:"cellMenu", for: indexPath) as! SideMenuCell
        
        cell.conf(itemName: tableArray[indexPath.row], img: imgArray[indexPath.row],cell: cell)
        
        
        
        
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Adoption"
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableArray[indexPath.row] {
        case "Adopta":
            performSegue(withIdentifier:SEGUE_ADOPTA, sender: nil)
        case "Historias":
            performSegue(withIdentifier: SEGUE_HISTORIAS, sender: nil)
        case "Mis Favoritos":
            performSegue(withIdentifier: SEGUE_MIS_FAVORITOS, sender: nil)
        case "Donaciones":
            performSegue(withIdentifier: SEGUE_DONACIONES, sender: nil)
        case "Configuracion":
            performSegue(withIdentifier: SEGUE_CONFIGURACION, sender: nil)
        case "Feedback":
            performSegue(withIdentifier: SEGUE_FEEDBACK, sender: nil)
        case "Logout":
            logout()
        default:
            break
        }
    }
    func logout(){
        UserDefaults.standard.removeObject(forKey: UID_KEY)
        dismiss(animated: true, completion: nil)
    }
    
    //prepare for segue
    func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if (segue.identifier == SEGUE_FEEDBACK) {
            
            
        }
    }
}
