//
//  DataService.swift
//  adoptMe
//
//  Created by Daniel Ramirez on 1/24/17.
//  Copyright © 2017 simpleCoding. All rights reserved.
//

import Foundation
import Firebase
import FBSDKCoreKit

let DB_BASE = FIRDatabase.database().reference()

class DataService{
    
    //servicio de informacion
    static let ds = DataService()
    
    fileprivate var _REF_BASE = DB_BASE
    fileprivate var _REF_USERS = DB_BASE.child("users")
    
    var REF_BASE:FIRDatabaseReference{
        return _REF_BASE
    }
    
    var REF_USERS:FIRDatabaseReference{
        return _REF_USERS
    }
    
    func createFirebaseDBUser(uid: String,userData:Dictionary<String,String>){
        REF_USERS.child(uid).updateChildValues(userData)
        
    }

}
