//
//  Organizacion.swift
//  adoptMe
//
//  Created by Daniel Ramirez on 2/8/17.
//  Copyright © 2017 simpleCoding. All rights reserved.
//

import Foundation

class ModeloOrganizacion{
    private var _id : Int!
    private var _direccion : String!
    private var _codigo_postal : Int!
    private var _pais : String!
    private var _estado : String!
    private var _proceso_adopcion : String!
    private var _pagina_web : String!
    private var _telefonos : String!
    private var _correo : String!
    private var _nombre : String!
    private var _latitud:Float!
    private var _longitud:Float!
    
    var latitud:Float{
        return _latitud
    }
    var longitud:Float{
        return _longitud
    }
    
    var id :Int{
        return _id
    }
    var nombre:String{
        return _nombre
    }

    var direccion:String{
        return _direccion
    }

    var codigo_postal:Int{
        return _codigo_postal
    }

    var pais:String{
        return _pais
    }

    var estado:String{
        return _estado
    }

    var proceso_adopcion :String{
        return _proceso_adopcion
    }

    var pagina_web :String{
        return _pagina_web
    }

    var telefonos: String{
        return _telefonos
    }

    var correo:String{
        return _correo
    }
    
    init(orgObj: Dictionary<String,AnyObject>){
        
        if let correo = orgObj["correo"] as? String{
            self._correo = correo
            
        }
        if let codigo_postal = orgObj["edad"] as? Int{
            self._codigo_postal = codigo_postal
            
        }
        if let direccion = orgObj["direccion"] as? String{
            self._direccion = direccion
            
        }
        if let estado = orgObj["estado"] as? String{
            self._estado = estado
            
        }
        if let id = orgObj["id"] as? Int{
            self._id = id
        }
        if let nombre = orgObj["nombre"] as? String{
            self._nombre = nombre
            
        }
        if let telefonos = orgObj["telefonos"] as? String{
            
            self._telefonos = telefonos
        }
        if let pagina_web = orgObj["pagina_web"] as? String{
            self._pagina_web = pagina_web
            
        }
        if let proceso_adopcion = orgObj["proceso_adopcion"] as? String{
            
            self._proceso_adopcion = proceso_adopcion
        }
        if let pais = orgObj["pais"] as? String{
            
            self._pais = pais
        }
    }
    init(){
        
    }

}
