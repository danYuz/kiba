//
//  ModeloThirdSpon.swift
//  adoptMe
//
//  Created by Daniel Ramirez on 4/17/17.
//  Copyright © 2017 techdevius. All rights reserved.
//

import Foundation

class ModelThirdSpon {

    private var _imageUrl:String!
    private var _renglon1: String!
    private var _renglon2: String!
    private var _renglon3: String!
    private var _renglon4: String!
    private var _encabezado: String!
    private var _subtitulo: String!
    private var _telefono:String!
    private var _ubicacion:String!
    private var _parrafo:String!
    private var _paginaWeb:String!
    private var _pieDePagUrl:String!
    
    
    var imageUrl:String{
        return _imageUrl
    }
    var renglon1:String{
        return _renglon1
    }
    var renglon2:String{
        return _renglon2
    }
    var renglon3:String{
        return _renglon3
    }
    var renglon4:String{
        return _renglon4
    }
    var encabezado:String{
        return _encabezado
    }
    var subtitulo:String{
        return _subtitulo
    }
    var ubicacion:String{
        return _ubicacion
    }
    var parrafo:String{
        return _parrafo
    }
    var paginaWeb:String{
        return _paginaWeb
    }
    var pieDePagUrl:String{
        return _paginaWeb
    }
    init(imageUrl:String,renglon1:String,renglon2:String,renglon3:String,renglon4:String,subtitulos:String,ubicacion:String,parrafo:String,paginaWeb:String,pieDePagUrl:String) {
        
        
        self._imageUrl = imageUrl
        self._renglon1 = renglon1
        self._renglon2 = renglon2
        self._renglon3 = renglon3
        self._renglon4 = renglon4
        self._subtitulo = subtitulos
        self._ubicacion = ubicacion
        self._parrafo = parrafo
        self._paginaWeb = paginaWeb
        self._pieDePagUrl = pieDePagUrl
        
        
    }
}
