//
//  Constants.swift
//  adoptMe
//
//  Created by Daniel Ramirez on 1/24/17.
//  Copyright © 2017 simpleCoding. All rights reserved.
//

import Foundation
let BASE = "Http://kibapp.org/"
let UID_KEY = "uid"
let REF_REUSABLE_ID = "refMainMenu"
let SHADOW_COLOR:CGFloat = 157.0/255.0
let ID_IMG_BORDER_COLOR = UIColor(red:99.0/255.0, green: 224.0/255.0, blue: 23.0/255.0, alpha: 1).cgColor
let ID_ARRAYDOGS = "id_arraydogs"
let NAMEAPPTITLE = "Kiba"

//urls

let mainViewDogsUrl = URL(string: "http://kibapp.org/BackEnd/AllPets")!
let mainSponsoresUrl = URL(string: "http://kibapp.org/BackEnd/AllAds")!

typealias JSONObject = [String:Any]
typealias DownloadCompleted = () -> ()


//segues MENU

let SEGUE_CONFIGURACION = "Configuracion"
let SEGUE_MIS_FAVORITOS = "Mis favoritos"
let SEGUE_HISTORIAS = "Historias"
let SEGUE_FEEDBACK = "Feedback"
let SEGUE_DONACIONES = "Donaciones"
let SEGUE_ADOPTA = "Adopta"


//segues
let TO_DETAIL_SEGUE = "ToDetailView"
