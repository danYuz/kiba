//
//  StyleViewLoginBtn.swift
//  adoptMe
//
//  Created by Daniel Ramirez on 3/15/17.
//  Copyright © 2017 techdevius. All rights reserved.
//

import UIKit

class StyleViewLoginBtn: UIView {

    override func awakeFromNib() {
        
        layer.cornerRadius = 5.0
        layer.shadowColor = UIColor(red:SHADOW_COLOR, green: SHADOW_COLOR, blue: SHADOW_COLOR, alpha: 0.5).cgColor
        layer.shadowRadius = 5.0
        layer.shadowOpacity = 0.8
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer.borderWidth = 4.0
        layer.borderColor = UIColor.white.cgColor
        self.tintColor = UIColor.white
    }

}
