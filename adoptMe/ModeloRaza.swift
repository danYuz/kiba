//
//  Raza.swift
//  adoptMe
//
//  Created by Daniel Ramirez on 2/8/17.
//  Copyright © 2017 simpleCoding. All rights reserved.
//

import Foundation

class ModeloRaza{
    public var _id : Int!
    public var _nombre_raza : String!
    public var _temperamento : String!
    public var _tamano : String!
    public var _cuidados : String!
    
    
    var id:Int{
        return _id
    }
    
    var nombre_raza:String{
        return _nombre_raza
    }
    var temperamento:String{
        return _temperamento
    }
    
    var tamano:String{
        return _tamano
    }
    
    var cuidados:String{
        return _cuidados
    }
    init() {
        
    }
    init(razaObj:Dictionary<String,AnyObject>){
        
        if let name = razaObj["nombre_raza"] as? String{
            self._nombre_raza = name
            
        }
        if let id = razaObj["id"] as? Int{
            self._id = id
            
        }
        if let temp = razaObj["temperamento"] as? String{
            self._temperamento = temp
            
        }
        if let tamano = razaObj["tamano"] as? String{
            self._tamano = tamano
            
        }
        if let cuidados = razaObj["cuidados"] as? String{
            self._cuidados = cuidados
            
        }
    }
}
